%global gem_name loofah
Name:                rubygem-%{gem_name}
Version:             2.2.3
Release:             1
Summary:             Manipulate and transform HTML/XML documents and fragments
License:             MIT
URL:                 https://github.com/flavorjones/loofah
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
Patch0:              rubygem-loofah-2.3.1-CVE-2019-15587-mitigate-XSS-vulnerability-in-SVG-animate-attributes.patch
BuildRequires:       ruby(release) rubygems-devel rubygem(nokogiri) >= 1.6.6.2 rubygem(minitest)
BuildRequires:       rubygem(crass)
BuildArch:           noarch
%description
Loofah is a general library for manipulating and transforming HTML/XML
documents and fragments. It's built on top of Nokogiri and libxml2, so
it's fast and has a nice API.
Loofah excels at HTML sanitization (XSS prevention). It includes some
nice HTML sanitizers, which are based on HTML5lib's whitelist, so it
most likely won't make your codes less secure.

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}
%patch0 -p1

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
ruby -Itest -e 'Dir.glob "./test/**/test_*.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.*
%license %{gem_instdir}/MIT-LICENSE.txt
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/Manifest.txt
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%doc %{gem_instdir}/SECURITY.md
%{gem_instdir}/benchmark
%{gem_instdir}/test

%changelog
* Tue Aug 18 2020 geyanan <geyanan2@huawei.com> - 2.2.3-1
- package init
